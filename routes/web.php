<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('checklists', ['uses' => 'ChecklistController@all']);
$router->get('checklist/{id}', ['as' => 'checklist', 'uses' => 'ChecklistController@show']);
$router->put('checklist/{id}', ['uses' => 'ChecklistController@update']);
$router->delete('checklist/{id}', ['uses' => 'ChecklistController@destroy']);
$router->post('checklist', ['uses' => 'ChecklistController@store']);
