<?php

class ChecklistTest extends TestCase
{
    /**
     * product [POST]
     */
    public function insertData()
    {
        $parameters = [
            'object_domain' => 'contact',
            'object_id' => '1',
            'due' => '2019-01-25 07:50:14',
            'urgency' => '1',
            'description' => 'Test Case Insert',
            'items' => [
                'Item A',
                'Item B',
                'Item C',
            ],
            'task_id' => '123'
        ];

        $this->post("checklist", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => [
                'type',
                'id',
                'attributes' => [
                    'object_domain',
                    'object_id',
                    'due',
                    'urgency',
                    'description',
                    'task_id',
                    'updated_at',
                    'created_at'
                ],
                'links' => [
                    'self'
                ]
            ]
        ]);
    }
}