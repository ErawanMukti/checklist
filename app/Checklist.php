<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    protected $guarded = [];

    protected $hidden = ['id'];

    public function items()
    {
    	return $this->hasMany('App\Item', 'checklist_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($user) {
             $user->items()->delete();
        });
    }

    public function paginateChecklist(array $filter)
    {
        $query = $this->newQuery();
        $limit = 10;

        if (isset($filter['id'])) {
            $query->where('id', $filter['id']);
        }
        if (isset($filter['include'])) {
            $query->with('items');
        }
        if (isset($filter['page_limit'])) {
            $limit = $filter['page_limit'];
        }

        return $query->paginate($limit);
    }

}