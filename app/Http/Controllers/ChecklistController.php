<?php

namespace App\Http\Controllers;

use App\Libs\Services\ChecklistService;
use App\Libs\Services\ItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChecklistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function all(Request $request, ChecklistService $service)
    {
        $filter = $request->all();
        $checklist = $service->getChecklist($filter);        

        $data = [];
        foreach($checklist as $key => $value) {
            $data[] = [
                'type' => 'checklist',
                'id' => $value->id,
                'attributes' => $value,
                'links' => ['self' => route('checklist', ['id' => $value->id])]
            ];
        }

        return response()->json([
            'meta' => [
                'count' => $checklist->count(),
                'total' => $checklist->total()
            ],
            'links' => [
                'first' => $checklist->url(1),
                'last' => $checklist->url($checklist->lastPage()),
                'next' => $checklist->nextPageUrl(),
                'prev' => $checklist->previousPageUrl()
            ],
            'data' => $data
        ]);
    }

    public function destroy($id, ChecklistService $service)
    {
        $service->destroyChecklist($id);
        return response(null, 204);
    }

    public function show($id, Request $request, ChecklistService $service)
    {
        $filter = $request->all();
        $filter['id'] = $id;
        $checklist = $service->getChecklist($filter);

        return response()->json([
            'type' => 'checklist',
            'id' => $id,
            'attributes' => $checklist[0],
            'links' => ['self' => route('checklist', ['id' => $checklist[0]->id])]
        ]);
    }

    public function store(Request $request,
                          ChecklistService $cService,
                          ItemService $iService) {
        $data['checklist'] = $request->except(['items']);
        $data['items'] = $request->only(['items']);

        $checklist = null;
        DB::transaction(function () use ($data, $cService, $iService, &$checklist) {
            $checklist = $cService->createChecklist($data['checklist']);

            $id = $checklist->getKey();
            foreach($data['items']['items'] as $item) {
                $iService->createItem([
                    'checklist_id' => $id,
                    'description' => $item
                ]);
            }
        });

        return response()->json([
            'data' => [
                'type' => 'checklists',
                'id' => $checklist->getKey(),
                'attributes' => $checklist,
                'links' => ['self' => route('checklist', ['id'=>$checklist->getKey()])]
            ]
        ]);
    }

    public function update($id, Request $request, ChecklistService $service)
    {
        $data = $request->all();
        $service->updateChecklist($id, $data);
        $checklist = $service->getChecklist(['id' => $id]);

        return response()->json([
            'type' => 'checklist',
            'id' => $id,
            'attributes' => $checklist[0],
            'links' => ['self' => route('checklist', ['id' => $checklist[0]->id])]
        ]);
    }
}
