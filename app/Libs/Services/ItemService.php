<?php

namespace App\Libs\Services;

use App\Item;
use App\Libs\Contracts\ItemContract;

class ItemService implements ItemContract
{
    private $model;

    public function __construct(Item $model)
    {
        $this->model = $model;
    }

    public function createItem($data)
    {
        return $this->model->create($data);
    }

    public function destroyItem($id)
    {
    	$this->model->findOrFail($id)->delete();
    }

    public function getItem($filter)
    {
    	return $this->model->limit(10);
    }

    public function updateItem($id, $data)
    {
        return $this->model->find($id)->update($data);
    }
}
