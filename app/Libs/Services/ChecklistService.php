<?php

namespace App\Libs\Services;

use App\Checklist;
use App\Libs\Contracts\ChecklistContract;

class ChecklistService implements ChecklistContract
{
    private $model;

    public function __construct(Checklist $model)
    {
        $this->model = $model;
    }

    public function createChecklist($data)
    {
        return $this->model->create($data);
    }

    public function destroyChecklist($id)
    {
    	$this->model->findOrFail($id)->delete();
    }

    public function getChecklist($filter)
    {
    	return $this->model->paginateChecklist($filter);
    }

    public function updateChecklist($id, $data)
    {
        return $this->model->find($id)->update($data);
    }
}
