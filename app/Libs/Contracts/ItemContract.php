<?php

namespace App\Libs\Contracts;

interface ItemContract
{
    public function createItem(array $data);

    public function destroyItem(int $id);

    public function getItem(array $filter);

    public function updateItem(int $id, array $data);
}