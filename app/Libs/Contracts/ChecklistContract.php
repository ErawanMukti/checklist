<?php

namespace App\Libs\Contracts;

interface ChecklistContract
{
    public function createChecklist(array $data);

    public function destroyChecklist(int $id);

    public function getChecklist(array $filter);

    public function updateChecklist(int $id, array $data);
}